// Karma configuration
// Generated on Sat May 13 2017 22:49:37 GMT-0700 (PDT)

module.exports = function(config) {

  var browserify = "browserify";
  var testFiles = "tests/react/**/*.spec.jsx";
  var sourceFiles = "src/react/**/*.jsx";
  var preProcessors = [browserify];
  var paths = [testFiles];

  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: "",

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ["mocha", browserify],

    // list of files / patterns to load in the browser
    files: paths,


    // list of files to exclude
    exclude: [],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      [sourceFiles]: preProcessors.concat(["coverage"]),
      [testFiles]: preProcessors,
    },

    browserify: {
      debug: true,
      transform: [['babelify', { "presets": ["env", "react"] }]],
      sourceType: "module",
      paths: paths,
    },

    // test results reporter to use
    // possible values: "dots", "progress"
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ["progress", "coverage"],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ["Chrome"], // You may use "ChromeCanary", "Chromium" or any other supported browser

    // you can define custom flags
    // customLaunchers: {
    //     Chrome_without_security: {
    //         base: "Chrome",
    //         flags: ["--disable-web-security"]
    //     }
    // },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

    plugins:[
      "karma-spec-reporter",
      "karma-coverage",
      "karma-mocha",
      "karma-browserify",
      "karma-chrome-launcher",
      // "karma-firefox-launcher,",
      // "karma-phantomjs-launcher",
      // "karma-webpack",
    ]
  })
};
