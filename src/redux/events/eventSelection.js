export default function(state={}, action) {
  const actionId = action.id;
  switch (action.type) {
    case "SELECT_EVENT":
      return Object.assign({}, state, {[actionId]: !state[actionId]});
    case "REMOVE_EVENT":
      return Object.keys(state).reduce((events, selected) => {
        if (`${actionId}` !== `${selected}`) {
          events[selected] = state[selected];
        }
        return events;
      }, {});
    default:
      return state;
  }
}
