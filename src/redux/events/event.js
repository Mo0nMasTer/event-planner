export default function(state=[], action) {
  const actionId = action.id;
  switch (action.type) {
    case "CREATE_EVENT":
      return [...state, action];
    case "REMOVE_EVENT":
      return state.filter(({id}) => !action.selected[id]);
    case "UPDATE_EVENT":
      return state.map((event) => {
        return event.id === actionId ? Object.assign(event, action) : event
      });
    default:
      return state;
  }
}