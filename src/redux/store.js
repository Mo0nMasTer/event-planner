import {createStore, combineReducers} from "redux";
import events from "./events/event";
import selected from "./events/eventSelection";
export default (function() {
  return createStore(combineReducers({events, selected}));
}());
