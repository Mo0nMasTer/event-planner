export default (object, methods) => methods.forEach((method) => {
  object[method] = object[method].bind(object);
});