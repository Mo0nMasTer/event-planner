import babelify from "babelify";
import browserify from "browserify";
import fs from "fs";

export default function compiler(pathToInputFile) {
  let output;
  let globalOptions;
  const bundler = browserify({
    cache: {},
    debug: true,
    entries: [pathToInputFile],
    packageCache: {},
  });
  const compile = (pathToOutputFile) => {
    bundler.bundle().pipe(fs.createWriteStream(pathToOutputFile));
  };
  return {
    compile(pathToOutputFile, options) {
      globalOptions = options;
      output = pathToOutputFile;
      compile(pathToOutputFile, options);
      return this;
    },
    babelify(options) {
      bundler.transform(babelify.configure(options));
      return this;
    },
  };
}
