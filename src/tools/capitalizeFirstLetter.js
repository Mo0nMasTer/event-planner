export default function(element) {
  return element[0].toUpperCase() + element.slice(1);
}
