import express from "express";
import {createServer} from "http";
import compiler from "../tools/compiler";
import path from "path";
const app = express();
const port = 8910;
const ip = "127.0.0.1";
const pathToPublicFiles = path.resolve(`${__dirname}/../../public`);
const pathToInputFile = path.resolve(`${__dirname}/../react/main.jsx`);
const pathToOutputFile = `${pathToPublicFiles}/index.js`;
const staticFileDirectory = express.static(pathToPublicFiles);
const server = createServer(app);
compiler(pathToInputFile)
  .babelify({
    extensions: [".js", ".jsx"],
    presets: ["env", "react"],
  })
  .compile(pathToOutputFile);
app.use(staticFileDirectory);
app.use(express.json());
app.get("/", (_, res) => res.sendFile(pathToOutputFile));
server.listen(port, ip, () => console.log(`  - listening for requests @ ${ip}:${port}`));
