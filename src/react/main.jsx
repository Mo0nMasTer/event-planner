import render from "./render.jsx";
import store from "../redux/store";
import events from "./events/eventControl.jsx"

window.onload = () => {
  store.subscribe(() => render(events));
  render(events);
};
