import React from "react";
import Events from "./eventList.jsx";
import Editor from "./eventEditor.jsx";
import store from "../../redux/store";
export default () => {
  const state = store.getState();
  const select = (event) => store.dispatch(Object.assign(event, {type: "SELECT_EVENT"}));
  return <div>
    <Editor/>
    <Events id={"event-list"} select={select} elements={state.events} {...state}/>
  </div>
}
