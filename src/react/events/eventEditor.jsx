import React, {Component} from "react";
import store from "../../redux/store";
import capitalizeFirstLetter from "../../tools/capitalizeFirstLetter";
import bindMethodsToClass from "../../tools/bind";
export default class Events extends Component {
  constructor() {
    super();
    this.state = {};
    this.id = 0;
    bindMethodsToClass(this, ["createEvent", "removeEvent"]);
    ["name", "location", "date", "time"].forEach((parameter) => {
      this[`get${capitalizeFirstLetter(parameter)}`] = (function (event) {
        this.setState({[parameter]: event.target.value});
      }).bind(this);
    });
  }
  createEvent() {
    const {time, name, location} = this.state;
    const type = "CREATE_EVENT";
    store.dispatch( {id: ++this.id, location, time, name, type});
  }
  removeEvent() {
    const {selected} = store.getState();
    store.dispatch({selected, type: "REMOVE_EVENT"});
  }
  render() {
    return <div id="create-event">
      <h1>Create an event:</h1>
      <p>name: <input onChange={this.getName}/></p>
      <p>time: <input type="date" onChange={this.getDate}/>
        <input type="time" onChange={this.getTime}/>
      </p>
      <p>location: <input onChange={(event) => this.getLocation(event)}/></p>
      <button onClick={this.createEvent}>add</button>
      <button onClick={this.removeEvent}>remove</button>
    </div>;
  }
}