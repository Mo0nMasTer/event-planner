import React from "react";
import Event from "./event.jsx";
export default function({elements, select, selected, id}) {
  const color = "DeepSkyBlue";
  return <div id={id}>
    <ul id={"events"}>{elements.map((element, index) => {
      const backgroundColor = selected[element.id] ? color : null;
      return <Event {...element} style={{backgroundColor}} onClick={() => select(element)} key={index + 1}/>;
    })}</ul>
  </div>;
}
