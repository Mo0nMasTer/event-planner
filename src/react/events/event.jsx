import React from "react";
export default function ({id, style, onClick, name, location, date, time}) {
  return <li id={id} style={style} onClick={onClick}>
    <p>name: {name}</p>
    <p>location: {location}</p>
    <p>time: {date} at {time}</p>
  </li>;
}