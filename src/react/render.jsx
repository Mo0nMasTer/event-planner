import ReactDOM from 'react-dom';
import React from 'react';

export default function(element) {
  ReactDOM.render(
    React.createElement(element),
    document.getElementById('app')
  );
}
