import {expect} from "chai";
import eventsReducer from "../../../src/redux/events/event";

describe("event", () => {
  let id = 1;
  const name = "Dance Dance Dance!";
  const location = "Denny's";
  const time = "now";
  const date = "september";
  const expectedEvent = {name, id, location, date, time};
  const input = (type) => Object.assign({type}, expectedEvent);
  it("Will update the app state with a new event.", () => {
    const event = input("CREATE_EVENT");
    expect(eventsReducer([], event)).to.deep.equal([event]);
  });
  it("Removes an event", () => {
    const state = eventsReducer([], input("CREATE_EVENT"));
    const removed = input("REMOVE_EVENT");
    removed.selected = {[id]: true};
    expect( eventsReducer(state, removed)).to.deep.equal([]);
  });
  it("Updates an event", () => {
    const changes =  {name: "You", time: "Tonight"};
    const state = eventsReducer([], input("CREATE_EVENT"));
    const changed = Object.assign(input("UPDATE_EVENT"), changes);
    expect(eventsReducer(state, changed))
      .to.deep.equal([changed]);
  });
});