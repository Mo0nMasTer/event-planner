import {expect} from "chai";
import eventSelection from "../../../src/redux/events/eventSelection";
describe("eventSelection", () => {
  const id = 1;
  it("Marks an element as selected or unselected", () => {
    expect(eventSelection({}, {id, type: "SELECT_EVENT"})).to.deep.equal({[id]: true});
  });
  it("Deselects an element when it is removed.", () => {
    const state = eventSelection({}, {id, type: "SELECT_EVENT"});
    expect(eventSelection(state, {id, type: "REMOVE_EVENT"})).to.deep.equal({});
  });
});
