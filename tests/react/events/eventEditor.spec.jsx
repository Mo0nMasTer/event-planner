import {expect} from "chai";
import React from "react";
import createReactContainer from "../../utilities/createReactContainer";
import render from "../../../src/react/render.jsx";
import eventEditor from "../../../src/react/events/eventEditor.jsx";

describe("eventEditor", () => {
  const appId = "app";
  const type = "div";
  const controls = [
    "title",
    "nameInput",
    "dateTimeInput",
    "locationInput",
    "addButton",
    "removeButton",
  ];
  it("Creates an interface for creating and modifying events", () => {
    let editor;
    let editorChildElements;
    let children;
    createReactContainer(appId, type);
    render(eventEditor);
    editor = document.getElementById("create-event");
    editorChildElements = editor.children;
    children = controls.reduce((elements, control, index) => {
      elements[control] = editorChildElements[index];
      return elements;
    }, {});
    controls.forEach((control) => expect(!children[control]).to.equal(false));
  });
});
