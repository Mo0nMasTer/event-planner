import {expect} from "chai";
import React from "react";
import events from "../../../src/react/events/eventControl.jsx";
import render from "../../../src/react/render.jsx";
import appStore from "../../../src/redux/store";
import createReactContainer from "../../utilities/createReactContainer";
describe("eventDisplay", () => {
  const appId = "app";
  const type = "div";
  const eventsCreatorId = "create-event";
  const eventListId = "events";
  const controls = [
    "title",
    "nameInput",
    "dateTimeInput",
    "locationInput",
    "addButton",
    "removeButton",
  ];
  const removeChildren = (element, removeButton) => {
    const childrenOfElement = element.children;
    let l = childrenOfElement.length;
    while (l--) {
      element.children[l].click();
    }
    removeButton.click();
  };
  let children;
  let eventEditor;
  let container;
  let editorChildElements;
  let addButton;
  let eventList;
  let removeButton;
  appStore.subscribe(() => render(events));
  beforeEach(() => {
    container = createReactContainer(appId, type);
    render(events);
    eventList = document.getElementById(eventListId);
    eventEditor = document.getElementById(eventsCreatorId);
    editorChildElements = eventEditor.children;
    children = controls.reduce((elements, control, index) => {
      elements[control] = editorChildElements[index];
      return elements;
    }, {});
    addButton = children.addButton;
    removeButton = children.removeButton;
  });
  afterEach(() => removeChildren(eventList, removeButton));
  it("Can add an events to its events list", () => {
    const eventChildren = eventList.children;
    addButton.click();
    expect(eventChildren.length).to.equal(1);
    addButton.click();
    expect(eventChildren.length).to.equal(2);
  });
  it("Can select or deselect events from its events list", () => {
    let child;
    addButton.click();
    child = eventList.children[0];
    child.click();
    expect(child.style.backgroundColor).to.equal("deepskyblue");
    child.click();
    expect(child.style.backgroundColor).to.equal("");
  });
  it("Can remove events from its events list", () => {
    const eventChildren = eventList.children;
    let childOne, childThree;
    addButton.click();
    expect(eventChildren.length).to.equal(1);
    childOne = eventChildren[0];
    addButton.click();
    expect(eventChildren.length).to.equal(2);
    addButton.click();
    expect(eventChildren.length).to.equal(3);
    childThree = eventChildren[2];
    childThree.click();
    childOne.click();
    removeButton.click();
    expect(eventChildren.length).to.equal(1);
  });
});