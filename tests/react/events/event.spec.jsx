import {expect} from "chai";
import React from "react";
import Event from "../../../src/react/events/event.jsx";
import createReactContainer from "../../utilities/createReactContainer";
import render from "../../../src/react/render.jsx";

describe("event", () => {
  const type = "ul";
  const appId = "app";
  const key = 1;
  const eventId = "testEvent";
  const eventDetails = {
    name: "testing",
    date: "this friday",
    time: "test time",
    location: "tim's moms"
  };
  beforeEach(() => createReactContainer(appId, type));
  it("Creates an element for displaying an event", () => {
    const dateAndTime = `time: ${eventDetails.date} at ${eventDetails.time}`;
    const event = () => <ul><Event id={eventId} {...eventDetails} key={key} /></ul>;
    let children, element;
    render(event);
    element = document.getElementById(eventId);
    children = element.children;
    expect(children[0].textContent).to.equal(`name: ${eventDetails.name}`);
    expect(children[1].textContent).to.equal(`location: ${eventDetails.location}`);
    expect(children[2].textContent).to.equal(dateAndTime);
  });
});