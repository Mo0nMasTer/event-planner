import {expect} from "chai";
import React from "react";
import render from "../../../src/react/render.jsx";
import Events from "../../../src/react/events/eventList.jsx";
import createReactContainer from "../../utilities/createReactContainer";

describe('eventList', function () {
  it("Creates an element for displaying events", () => {
    const id = "event-list";
    let events;
    createReactContainer("app", "div");
    render(() => <Events id={id} elements={[]} selected={{}}/>);
    events = document.getElementById(id);
    expect(events.children[0].id).to.equal("events");
  });
  it("Displays a no background color when not selected", () => {
    const listId = "event-list";
    const id = 1;
    let events;
    createReactContainer("app", "div");
    render(() => <Events id={listId} elements={[{id}]} selected={{}}/>);
    events = document.getElementById(listId).children[0];
    expect(events.children[0].style.backgroundColor).to.equal('');

  });
  it("Displays a background color when selected", () => {
    const listId = "event-list";
    const id = 1;
    let events;
    createReactContainer("app", "div");
    render(() => <Events id={listId} elements={[{id}]} selected={{[id]: true}}/>);
    events = document.getElementById(listId).children[0];
    expect(events.children[0].style.backgroundColor).to.equal("deepskyblue");
  });
});
