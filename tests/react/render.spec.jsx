import {expect} from "chai";
import React from "react";
import render from "../../src/react/render.jsx";
describe("render", () => {
  const id = "testComponent";
  const value = "testValue";
  const type = "div";
  const component = () => <div id={id}>{value}</div>;
  it("Renders the supplied react component.", () => {
    const element = document.createElement(type);
    let createdComponent;
    element.setAttribute("id", "app");
    document.body.appendChild(element);
    render(component);
    createdComponent = document.getElementById(id);
    expect(createdComponent.tagName).to.equal(type.toUpperCase());
    expect(createdComponent.textContent).to.equal(value);
  });
});